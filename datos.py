from tkinter import Tk, Text, messagebox, Button
import pandas as pd
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori, association_rules

def calcular_reglas_asociacion():
    # Obtener los datos del cuadro de texto
    data = entrada_texto.get("1.0", "end-1c")
    # Convertir los datos en una lista de listas
    dataset = [line.split() for line in data.splitlines() if line]

    try:
        # Convertir los datos en una matriz binaria
        te = TransactionEncoder()
        te_ary = te.fit(dataset).transform(dataset)
        df = pd.DataFrame(te_ary, columns=te.columns_)

        # Calcular los conjuntos de elementos frecuentes con el algoritmo Apriori
        frequent_itemsets = apriori(df, min_support=0.2, use_colnames=True)

        # Generar las reglas de asociación
        rules = association_rules(frequent_itemsets, metric="lift", min_threshold=1)

        # Crear una cadena de texto con el cálculo de soporte, confianza y lift
        resultado = ""
        for _, rule in rules.iterrows():
            antecedents = ', '.join(list(rule['antecedents']))
            consequents = ', '.join(list(rule['consequents']))
            support = rule['support']
            confidence = rule['confidence']
            lift = rule['lift']
            resultado += f"Regla: {antecedents} -> {consequents}\n"
            resultado += f"Soporte: {support}\n"
            resultado += f"Confianza: {confidence}\n"
            resultado += f"Lift: {lift}\n"
            resultado += "----------------------\n"

        # Mostrar el resultado en una ventana emergente
        messagebox.showinfo("Resultado", resultado)

    except Exception as e:
        messagebox.showerror("Error", str(e))

# Crear la ventana principal
ventana = Tk()
ventana.title("Reglas de Asociación")
ventana.geometry("400x300")

# Crear un cuadro de texto para ingresar los datos
entrada_texto = Text(ventana, height=10, width=40)
entrada_texto.pack()

# Crear un botón para calcular las reglas de asociación
boton_calcular = Button(ventana, text="Calcular", command=calcular_reglas_asociacion)
boton_calcular.pack()

# Ejecutar la ventana principal
ventana.mainloop()
